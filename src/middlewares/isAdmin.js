const AuthError = require('../errors/AuthError');

const isAdmin = (req,res,next) => {
    if(req.headers['authorization'] !== '123'){
        next(new AuthError());
    }else{
        next()
    }
}

module.exports = { isAdmin }