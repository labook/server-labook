const errorMiddleware = (error, req, res, next) => {
    console.log(error)
    console.log('ERROR HANDLER')
    let errorObject;
    if (error.toJson) {
        errorObject = error.toJson();
    } else {
        errorObject = {
            status: 500,
            name: 'UnknownError',
            message: error.message
        }
    }

    res.status(errorObject.status).send(errorObject)
}

module.exports = { errorMiddleware } 