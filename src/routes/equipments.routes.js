const { Router } = require('express');
const router = Router();
const { getEquipments, getEquipment, createEquipment, deleteEquipment, updateEquipment } = require('../controllers/equipments.controllers');
const { equipmentSchema, equipmentExist, equipmentNotExist, paginationValidator } = require('../coordinator/validateFunctions');
const { validate } = require('../coordinator/validate');
const { isAdmin } = require('../middlewares/isAdmin');
const { validateJwt } = require('../helpers/validate_jwt');

router.get('/equipments',
  [
    validateJwt,
    validate(paginationValidator),
  ],
  getEquipments
);
router.get('/equipment/:id',
  [
    validateJwt,
    validate(equipmentExist),
  ],
  getEquipment
);
router.post('/add-equipment',
  // isAdmin,
  // validate(equipmentSchema),
  [
    validateJwt,
    validate(equipmentNotExist),
  ],
  createEquipment
);
router.put('/update-equipment/:id',
  // isAdmin,
  // validate(equipmentSchema),
  [
    validateJwt,
    validate(equipmentExist),
  ],
  updateEquipment
);
router.delete('/remove-equipment/:id',
  // isAdmin,
  [
    validateJwt,
    validate(equipmentExist),
  ],
  deleteEquipment
);

module.exports = router;