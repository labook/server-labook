const { Router } = require('express');
const router = Router();
const { getReagentsSelection, createReagentSelection, getReagentSelection, updateReagentSelection, removeReagentSelection } = require('../controllers/reagentSelection.controllers');
const { reagentSelectionSchema, reagentSelectionExist } = require('../coordinator/validateFunctions');
const { validate } = require('../coordinator/validate');
const { validateJwt } = require('../helpers/validate_jwt');


router.get('/reagentsSelection',
    [
        validateJwt,
    ],
    getReagentsSelection
);
router.get('/reagentSelection/:id',
    [
        validateJwt,
        validate(reagentSelectionExist),
    ],
    getReagentSelection
);
router.post('/add-reagentSelection',
    // validate(reagentSelectionSchema),
    [
        validateJwt,
    ],
    createReagentSelection
);
router.put('/update-reagentSelection/:id',
    [
        validateJwt,
        validate(reagentSelectionExist),
    ],
    updateReagentSelection
);
router.delete('/remove-reagentSelection/:id',
    [
        validateJwt,
        validate(reagentSelectionExist),
    ],
    removeReagentSelection
);


module.exports = router;