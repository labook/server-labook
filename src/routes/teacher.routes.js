const { Router } = require('express');
const router = Router();
const { getTeachers, getTeacher, createTeacher, updateTeacher, removeTeacher } = require('../controllers/teachers.controllers');
const { teacherSchema } = require('../coordinator/validateFunctions');
const { validate } = require('../coordinator/validate');
const { isAdmin } = require('../middlewares/isAdmin');
const { teacherExist } = require('../coordinator/teacher.coordinator');
const { validateJwt } = require('../helpers/validate_jwt');

router.get('/teachers',
    [
        validateJwt,
    ],
    getTeachers
);
router.get('/teacher/:id',
    [
        validateJwt,
        validate(teacherExist),
    ],
    getTeacher
);
router.post('/add-teacher',
    // isAdmin,
    [
        validateJwt,
        validate(teacherSchema),
    ],
    createTeacher
);
router.put('/update-teacher/:id',
    // isAdmin,
    // validate(teacherSchema),
    [
        validateJwt,
        validate(teacherExist),
    ],
    updateTeacher
);
router.delete('/remove-teacher/:id',
    // isAdmin,
    [
        validateJwt,
        validate(teacherExist),
    ],
    removeTeacher
);

module.exports = router;