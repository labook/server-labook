const { Router } = require('express');
const router = Router();
const { getGroupResearch, createGroupResearch, getGroupResearchById } = require('../controllers/groupResearch.controllers');
const { validateJwt } = require('../helpers/validate_jwt');

router.get('/groupResearchs',
    [
        validateJwt,
    ],
    getGroupResearch
);
router.get('/groupResearch/:id',
    [
        validateJwt,
    ],
    getGroupResearchById
);
router.post('/add-groupResearch',
    [
        validateJwt,
    ],
    createGroupResearch
);



module.exports = router