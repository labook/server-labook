const { Router } = require('express');
const router = Router();
const { getRols, getRol, createRol, updateRol, removeRol } = require('../../controllers/authControllers/rol.controller');


router.get('/rols',
    getRols
);
router.get('/rol/:id',
    getRol
);
router.post('/add-rol',
    createRol
);
router.put('/update-rol/:id',
    updateRol
);
router.delete('/remove-rol/:id',
    removeRol
);


module.exports = router