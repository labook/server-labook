const { request } = require("express");

const { Router } = require('express');
const router = Router();
const { signUp, signIn, adminSignUp } = require('../../controllers/authControllers/auth.controllers');


router.post('/signUp',
    signUp
);
router.post('/signIn',
    signIn
)
router.post('/adminSignUp',
    adminSignUp
)


module.exports = router;