const { Router } = require('express');
const router = Router();
const { getEquipmentSelections, getEquipmentSelection, createEquipmentSelection, updateEquipmentSelection, removeEquipmentSelection } = require('../controllers/equipmentSelection.controllers');
const { equipmentSelectionExist } = require('../coordinator/validateFunctions');
const { validate } = require('../coordinator/validate');
const { validateJwt } = require('../helpers/validate_jwt');

router.get('/equipmentSelections',
    [
        validateJwt,
    ],
    getEquipmentSelections
);
router.get('/equipmentSelection/:id',
    [
        validateJwt,
        validate(equipmentSelectionExist),
    ],
    getEquipmentSelection
);
router.post('/add-equipmentSelection',
    [
        validateJwt,
    ],
    createEquipmentSelection
);
router.put('/update-equipmentSelection/:id',

    [
        validateJwt,
        validate(equipmentSelectionExist),
    ],
    updateEquipmentSelection
);
router.delete('/remove-equipmentSelection/:id',
    [
        validateJwt,
        validate(equipmentSelectionExist),
    ],
    removeEquipmentSelection
);


module.exports = router