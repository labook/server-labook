const { Router } = require('express');
const router = Router();
const { getStatements, getStatement, createStatement, updateStatement, removeStatement } = require('../controllers/statement.controllers');
const { statementSchema } = require('../coordinator/validateFunctions');
const { validate } = require('../coordinator/validate');
const { isAdmin } = require('../middlewares/isAdmin');
const { statementExist } = require('../coordinator/statement.coordinator');
const { validateJwt } = require('../helpers/validate_jwt');

router.get('/statements',
    [
        validateJwt,
    ],
    getStatements
);
router.get('/statement/:id',
    [
        validateJwt,
        validate(statementExist),
    ],
    getStatement
);
router.post('/add-statement',
    // isAdmin,
    // validate(statementSchema),
    [
        validateJwt,
    ],
    createStatement
);
router.put('/update-statement/:id',
    // isAdmin,
    [
        validateJwt,
        validate(statementExist),
    ],
    // validate(statementSchema),
    updateStatement
);
router.delete('/remove-statement/:id',
    // isAdmin,
    [
        validateJwt,
        validate(statementExist),
    ],
    removeStatement
);

module.exports = router