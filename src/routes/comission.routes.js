const { Router } = require('express');
const router = Router();
const { getComissions, getComission, createComission, updateComission, removeComission } = require('../controllers/comission.controllers');
const { comissionSchema } = require('../coordinator/validateFunctions');
const { comissionExist, comissionNotExist } = require('../coordinator/comission.coordinator');
const { validate } = require('../coordinator/validate');
const { isAdmin } = require('../middlewares/isAdmin');
const { validateJwt } = require('../helpers/validate_jwt');

router.get('/comissions',
    [
        validateJwt
    ],
    getComissions
);
router.get('/comission/:id',
    [
        validateJwt,
        validate(comissionExist),
    ],
    getComission
);
router.post('/add-comission',
    // isAdmin,
    // validate(comissionSchema),
    [
        validateJwt,
        validate(comissionNotExist),
    ],
    createComission
);
router.put('/update-comission/:id',
    // isAdmin,
    // validate(comissionSchema),
    [
        validateJwt,
        validate(comissionExist),
    ],
    updateComission
);
router.delete('/remove-comission/:id',
    // isAdmin,
    [
        validateJwt,
        validate(comissionExist),
    ],
    removeComission
);

module.exports = router;