const { Router } = require('express');
const router = Router();
const { getResearchs, getResearch, createResearch, updateResearch, removeResearch } = require('../controllers/research.controllers');
const { researchSchema } = require('../coordinator/validateFunctions');
const { validate } = require('../coordinator/validate');
const { isAdmin } = require('../middlewares/isAdmin');
const { researchNotExist, researchExist } = require('../coordinator/research.coordinator');
const { validateJwt } = require('../helpers/validate_jwt');


router.get('/researchs',
    [
        validateJwt,
    ],
    getResearchs
);
router.get('/research/:id',
    [
        validateJwt,
        validate(researchExist),
    ],
    getResearch
);
router.post('/add-research',
    // isAdmin,
    [
        validateJwt,
        validate(researchNotExist),
    ],
    // validate(researchSchema),
    createResearch
);
router.put('/update-research/:id',
    // isAdmin,
    [
        validateJwt,
        validate(researchExist),
    ],
    // validate(researchSchema),
    updateResearch
);
router.delete('/remove-research/:id',
    // isAdmin,
    [
        validateJwt,
        validate(researchExist),
    ],
    removeResearch
);

module.exports = router