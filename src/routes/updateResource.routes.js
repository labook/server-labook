const { Router } = require('express');
const router = Router();
const { updateResource } = require('../controllers/updateResource.controller');

router.put('/update-resource/:id/:type',
    updateResource
);


module.exports = router;