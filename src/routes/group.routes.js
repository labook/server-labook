const { Router } = require('express');
const router = Router();
const { getGroups, createGroup, getGroup, updateGroup, removeGroup } = require('../controllers/group.controllers.js');
const { groupSchema } = require('../coordinator/validateFunctions');
const { groupExist } = require('../coordinator/group.coordinator');
const { validate } = require('../coordinator/validate');
const { isAdmin } = require('../middlewares/isAdmin');
const { validateJwt } = require('../helpers/validate_jwt.js');

router.get('/groups',
    [
        validateJwt,
    ],
    getGroups
);
router.get('/group/:id',
    [
        validateJwt,
        validate(groupExist),
    ],
    getGroup
);
router.post('/add-group',
    // isAdmin,
    // validate(groupSchema),
    [
        validateJwt,
    ],
    createGroup
);
router.put('/upload-group/:id',
    // isAdmin,
    // validate(groupSchema),
    [
        validateJwt,
        validate(groupExist),
    ],
    updateGroup
);
router.delete('/remove-group/:id',
    // isAdmin,
    [
        validateJwt,
        validate(groupExist),
    ],
    removeGroup
);

module.exports = router;