const { Router } = require('express');
const router = Router();
const { getMaterialsSelection, createMaterialSelection, getMaterialSelection, updateMaterialSelection, removedMaterialSelection } = require('../controllers/materialSelection.controllers');
const { materialSelecionExist } = require('../coordinator/validateFunctions');
const { validate } = require('../coordinator/validate');
const { validateJwt } = require('../helpers/validate_jwt');

router.get('/materialsSelection',
    [
        validateJwt,
    ],
    getMaterialsSelection
);
router.get('/materialSelection/:id',
    [
        validateJwt,
        validate(materialSelecionExist),
    ],
    getMaterialSelection
);
router.post('/add-materialSelection',
    [
        validateJwt,
    ],
    createMaterialSelection
);
router.put('/update-materialSelection/:id',
    [
        validateJwt,
        validate(materialSelecionExist),
    ],
    updateMaterialSelection
);
router.delete('/remove-materialSelection/:id',
    [
        validateJwt,
        validate(materialSelecionExist),
    ],
    removedMaterialSelection
);


module.exports = router;

