const { Router } = require('express');
const router = Router();
const { getSafetySheets, getSafetySheet, createSafetySheet, updateSafetySheet, deleteSafetySheet } = require('../controllers/safetySheet.controllers');
const { equipmentSchema, equipmentExist, equipmentNotExist } = require('../coordinator/validateFunctions');
const { validate } = require('../coordinator/validate');
const { isAdmin } = require('../middlewares/isAdmin');
const { validateJwt } = require('../helpers/validate_jwt');

router.get('/safetySheets',
    [
        validateJwt,
    ],
    getSafetySheets
);
router.get('/safetySheet/:id',
    //validate(equipmentExist),
    [
        validateJwt,
    ],
    getSafetySheet
);
router.post('/add-safetySheet',
    // isAdmin,
    [
        validateJwt,
        validate(equipmentNotExist),
    ],
    //validate(equipmentSchema),
    createSafetySheet
);
router.put('/update-safetySheet/:id',
    // isAdmin,
    [
        validateJwt,
    ],
    updateSafetySheet
);
router.delete('/remove-safetySheet/:id',
    // isAdmin,
    [
        validateJwt,
    ],
    deleteSafetySheet
);

module.exports = router;