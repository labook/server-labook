const { Router } = require('express');
const router = Router();
const { getStudentsGroup, getStudentGroup, createStudentGroup, updateStudentGroup, removeStudentGroup } = require('../controllers/studentGroup.controller');
const { studentGroupSchema } = require('../coordinator/validateFunctions');
const { studentGroupExist } = require('../coordinator/studentGroup.coordinator');
const { validate } = require('../coordinator/validate');
const { isAdmin } = require('../middlewares/isAdmin');
const { validateJwt } = require('../helpers/validate_jwt');

router.get('/studentGroups',
    [
        validateJwt,
    ],
    getStudentsGroup
);
router.get('/studentGroup/:id',
    [
        validateJwt,
        validate(studentGroupExist),
    ],
    getStudentGroup
);
router.post('/add-studentGroup',
    // isAdmin,
    // validate(studentGroupSchema),
    [
        validateJwt,
    ],
    createStudentGroup
);
router.put('/update-studentGroup/:id',
    // isAdmin,
    [
        validateJwt,
        validate(studentGroupExist),
    ],
    // validate(studentGroupSchema),
    updateStudentGroup
);
router.delete('/remove-studentGroup/:id',
    // isAdmin,
    [
        validateJwt,
        validate(studentGroupExist),
    ],
    removeStudentGroup
);



module.exports = router