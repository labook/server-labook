const { Router } = require('express');
const router = Router();
const { getSubjects, getSubject, createSubject, updateSubject, removeSubject } = require('../controllers/subjects.controllers');
const { subjectSchema, subjectExist, subjectNotExist } = require('../coordinator/validateFunctions');
const { validate } = require('../coordinator/validate');
const { isAdmin } = require('../middlewares/isAdmin');
const { validateJwt } = require('../helpers/validate_jwt');

router.get('/subjects',
    [
        validateJwt
    ],
    getSubjects
);
router.get('/subject/:id',
    [
        validateJwt,
        validate(subjectExist),
    ],
    getSubject
);
router.post('/add-subject',
    // isAdmin,
    // validate(subjectSchema),
    [
        validateJwt,
        validate(subjectNotExist),
    ],
    createSubject
);
router.put('/update-subject/:id',
    // isAdmin,
    // validate(subjectSchema),
    [
        validateJwt,
        validate(subjectExist),
    ],
    updateSubject
);
router.delete('/remove-subject/:id',
    // isAdmin,
    [
        validateJwt,
        validate(subjectExist),
    ],
    removeSubject
);

module.exports = router;