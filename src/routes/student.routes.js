const { Router } = require('express');
const router = Router();
const { getStudents, getStudent, createStudent, updateStudent, removeStudent, getStatusStudent } = require('../controllers/student.controllers');
const { studentSchema } = require('../coordinator/validateFunctions');
const { studentExist, studentNotExist } = require('../coordinator/student.coordinator');
const { validate } = require('../coordinator/validate');
const { isAdmin } = require('../middlewares/isAdmin');
const { validateJwt } = require('../helpers/validate_jwt');

router.get('/student-status',
    [
        validateJwt,
    ],
    getStatusStudent
);

router.get('/students',
    [
        validateJwt,
    ],
    getStudents
);
router.get('/student/:id',
    [
        validateJwt,
        validate(studentExist),
    ],
    getStudent
);
router.post('/add-student',
    // isAdmin,
    [
        validateJwt,
        validate(studentNotExist),
    ],
    // validate(studentSchema),
    createStudent
);
router.put('/update-student/:id',
    // isAdmin,
    [
        validateJwt,
        validate(studentExist),
    ],
    // validate(studentSchema),
    updateStudent
);
router.delete('/remove-student/:id',
    // isAdmin,
    [
        validateJwt,
        validate(studentExist),
    ],
    removeStudent
);


module.exports = router