const { Router } = require('express');
const router = Router();
const { getTeachersComission, getTeacherComission, createTeacherComission, updateTeacherComission, removeTeacherComission } = require('../controllers/teacherComission.controllers');
const { teacherComissionSchema } = require('../coordinator/validateFunctions');
const { teacherComissionExist } = require('../coordinator/teacherComission.coordinator');
const { validate } = require('../coordinator/validate');
const { isAdmin } = require('../middlewares/isAdmin');
const { validateJwt } = require('../helpers/validate_jwt');
 
router.get('/teachersComission',
    [
        validateJwt,
    ],
    getTeachersComission
);
router.get('/teacherComission/:id',
    [
        validateJwt,
        validate(teacherComissionExist),
    ],
    getTeacherComission
);
router.post('/add-teacherComission',
    // isAdmin,
    // validate(teacherComissionSchema),
    [
        validateJwt,
    ],
    createTeacherComission
);
router.put('/update-teacherComission/:id',
    // isAdmin,
    [
        validateJwt,
        validate(teacherComissionExist),
    ],
    // validate(teacherComissionSchema),
    updateTeacherComission
);
router.delete('/remove-teacherComission/:id',
    // isAdmin,
    [
        validateJwt,
        validate(teacherComissionExist),
    ],
    removeTeacherComission
);

module.exports = router;