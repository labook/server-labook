//Import routes
const reagentsRoutes = require("./reagents.routes");
const materialsRoutes = require("./materials.routes");
const equipmentsRoutes = require("./equipments.routes");
const safetySheetsRoutes = require("./safetySheet.routes");
const materialSelectionsRoutes = require("./materialSelection.routes");
const reagentSelectionsRoutes = require("./reagentSelection.routes");
const equipmentSelectionsRoutes = require("./equipmentSelection.routes");
const groupsRoutes = require("./group.routes");
const subjectsRoutes = require("./subject.routes");
const comissionsRoutes = require("./comission.routes");
const studentsRoutes = require("./student.routes");
const teachersRoutes = require("./teacher.routes");
const teachersComissionRoutes = require("./teacherComission.routes");
const researchsRoutes = require("./research.routes");
const statementsRoutes = require("./statement.routes");
const studentGroupsRoutes = require("./studentGroup.routes");
const stepResearchRoutes = require("./step_research.routes");
const rolRoutes = require("./authRoutes/rol.routes");
const authRoutes = require("./authRoutes/login.routes");
const groupResearchRoutes = require("./groupResearch.routes");
const updateResourceRoutes = require("./updateResource.routes");

module.exports = [
  reagentsRoutes,
  materialsRoutes,
  equipmentsRoutes,
  safetySheetsRoutes,
  materialSelectionsRoutes,
  reagentSelectionsRoutes,
  equipmentSelectionsRoutes,
  groupsRoutes,
  subjectsRoutes,
  comissionsRoutes,
  studentsRoutes,
  teachersRoutes,
  teachersComissionRoutes,
  researchsRoutes,
  statementsRoutes,
  studentGroupsRoutes,
  stepResearchRoutes,
  rolRoutes,
  authRoutes,
  groupResearchRoutes,
  updateResourceRoutes,
];
