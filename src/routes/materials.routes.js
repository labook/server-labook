const { Router } = require('express');
const router = Router();
const { getMaterials, getMaterial, createMaterial, deleteMaterial, updateMaterial } = require('../controllers/materials.controllers');
const { materialSchema, materialExist, materialNotExist, paginationValidator } = require('../coordinator/validateFunctions');
const { validate } = require('../coordinator/validate');
const { isAdmin } = require('../middlewares/isAdmin');
const { validateJwt } = require('../helpers/validate_jwt');

router.get('/materials',
    [
        validateJwt,
        validate(paginationValidator),
    ],
    getMaterials
);
router.get('/material/:id',
    [
        validateJwt,
        validate(materialExist),
    ],
    getMaterial
);
router.post('/add-material',
    // isAdmin,
    // validate(materialNotExist),
    // validate(materialSchema),
    [
        validateJwt,
    ],
    createMaterial
);
router.put('/update-material/:id',
    // isAdmin,
    [
        validateJwt,
        validate(materialExist),
    ],
    // validate(materialSchema),
    updateMaterial
);
router.delete('/remove-material/:id',
    // isAdmin,
    [
        validateJwt,
        validate(materialExist),
    ],
    deleteMaterial
);

module.exports = router;
