const { Router } = require('express');
const router = Router();
const { getReagents, getReagent, createReagent, updateReagent, deleteReagent } = require('../controllers/reagents.controllers');
const { reagentSchema, reagentCasNotExist, reagentExist, paginationValidator } = require('../coordinator/validateFunctions');
const { validate } = require('../coordinator/validate');
const { isAdmin } = require('../middlewares/isAdmin');
const { validateJwt } = require('../helpers/validate_jwt');

router.get('/reagents',
    [
        validateJwt,
        validate(paginationValidator),
    ],
    getReagents
);
router.get('/reagent/:id',
    [
        validateJwt,
        validate(reagentExist),
    ],
    getReagent
);
router.post('/add-reagent',
    // isAdmin,
    [
        validateJwt,
        validate(reagentCasNotExist),
    ],
    // validate(reagentSchema),
    createReagent
);
router.put('/update-reagent/:id',
    // isAdmin,
    [
        validateJwt,
        validate(reagentExist),
    ],
    // validate(reagentSchema),
    updateReagent
);
router.delete('/remove-reagent/:id',
    // isAdmin,
    [
        validateJwt,
        validate(reagentExist),
    ],
    deleteReagent
);

module.exports = router;