const { Router } = require('express');
const router = Router();
const { getSteps, getStep, createStep, updateStep, deleteStep } = require('../controllers/stepResearch.controllers');
const { validateJwt } = require('../helpers/validate_jwt');


router.get('/stepsResearch',
    [
        validateJwt,
    ],
    getSteps
);
router.get('/stepResearch/:id',
    [
        validateJwt,
    ],
    getStep
);
router.post('/add-stepResearch',
    [
        validateJwt,
    ],
    createStep
);
router.put('/update-stepResearch/:id',
    [
        validateJwt,
    ],
    updateStep
);
router.delete('/remove-stepResearch/:id',
    [
        validateJwt,
    ],
    deleteStep
);


module.exports = router