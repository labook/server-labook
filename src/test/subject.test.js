const { getSubjects } = require('../controllers/subjects.controllers')
const request = require('supertest');
const app = require('../index');
const { expect } = require('chai');

describe('Test subject controllers', () => {
    it('Respond whit one subject and a list of comissions', (done) => {
        request(app)
            .get('/subject/2')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect({
                "id": 2,
                "name": "Biotecnologia 1",
                "comissions": []
            })
            .expect(200, done)
    })

    it('Respond with status 201 if the subject was created succesfully', done => {
        const data = {
            name: 'Test Subject',
        }
        request(app)
            .post('/add-subject')
            .send(data)
            .set('Accept', 'application/json')
            .set('authorization', '123')
            .expect('Content-Type', /json/)
            .expect(201, done)
    })

    it('Respod whit status 200 if the subject was deleted succesfully', (done) => {
        request(app)
            .delete('/remove-subject/5')
            .set('Accept', 'application/json')
            .set('authorization', '123')
            .expect(200, done)
    })

    it('Respond with a list containing subjects and a list of comissions', (done) => {
        request(app)
            .get('/subjects')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect([
                {
                    "id": 2,
                    "name": "Biotecnologia 1",
                    "comissions": []
                }
            ])
            .expect(200, done);
    });
})

