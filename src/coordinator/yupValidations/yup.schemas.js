const yup = require('yup');

const Reagent = yup.object().shape({
    name: yup.string().min(3).required(),
    image: yup.string().required(),
    cas: yup.number().min(0).max(9999).integer().required(),
    description: yup.string().min(5).required(),
});

const Material = yup.object().shape({
    name: yup.string().min(3).required(),
    image: yup.string().required(),
    description: yup.string().min(5).required(),
});

const Equipment = yup.object().shape({
    name: yup.string().min(3).required(),
    image: yup.string().required(),
    description: yup.string().min(5).required(),
});

const ReagentSelection = yup.object().shape({
    id_reagent_fk: yup.number().min(1).max(9999).integer().required(),
    id_group_fk: yup.number().min(1).max(9999).integer().required()
});

const MaterialSelection = yup.object().shape({
    id_material_fk: yup.number().min(1).max(9999).integer().required(),
    id_group_fk: yup.number().min(1).max(9999).integer().required()
});

const EquipmentSelection = yup.object().shape({
    id_equipment_fk: yup.number().min(1).max(9999).integer().required(),
    id_group_fk: yup.number().min(1).max(9999).integer().required()
});

const Subject = yup.object().shape({
    name: yup.string().min(3).required(),
});

const Comission = yup.object().shape({
    description: yup.string().min(5).required(),
    //start_time: validar fechas
    //end_time: mismo que start_time
    // days: modificar a array, aun no se valida
    id_subject_fk: yup.number().min(1).max(9999).integer().required(),
    // start_date: fecha
    //end_time: fecha
    active: yup.boolean().required()
});

const Student = yup.object().shape({
    name: yup.string().min(1).required(),
    lastname: yup.string().min(1).required(),
    dni: yup.number().integer().required(),
    id_comission_fk: yup.number().min(1).max(9999).integer().required(),
});

const Group = yup.object().shape({
    name: yup.string().min(3).required(),
    id_research_fk: yup.number().min(1).max(9999).integer(),
    id_comission_fk: yup.number().min(1).max(9999).integer(),
});

const Student_Group = yup.object().shape({
    id_student_fk: yup.number().min(1).max(9999).integer().required(),
    id_group_fk: yup.number().min(1).max(9999).integer().required(),
});

const Teacher = yup.object().shape({
    name: yup.string().min(1).required(),
    lastname: yup.string().min(1).required(),
});

const Teacher_Comission = yup.object().shape({
    is_holder: yup.boolean().required(),
    id_comission_fk: yup.number().min(1).max(9999).integer().required(),
    id_teacher_fk: yup.number().min(1).max(9999).integer().required(),
    id_research_fk: yup.number().min(1).max(9999).integer().required()
});

const Research = yup.object().shape({
    name: yup.string().min(1).max(30).required(),
    // deliver_date: fecha
});

const Statement = yup.object().shape({
    // statement_pdf: correjir tipo de dato antes de validar
    id_research_fk: yup.number().min(1).max(9999).integer().required()
});


module.exports = { Reagent, Material, Equipment, ReagentSelection, MaterialSelection, EquipmentSelection, Subject, Comission, Student, Group, Teacher, Teacher_Comission, Research, Statement, Student_Group }
