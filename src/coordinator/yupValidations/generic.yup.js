const yup = require('yup');
const ExistError = require('../../errors/ExistError');
let errorObject = new Object()

const schemaValidator = (schema, data) => {
    try {
        schema.validateSync(data.body);
    } catch (err) {
        errorObject.message = err.message
        errorObject.status = 400
        errorObject.path = err.path
        throw new ExistError(errorObject)
    }
}


module.exports = { schemaValidator }
