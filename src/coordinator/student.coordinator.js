const models = require('../models');
const ExistError = require('../errors/ExistError');
const { resourceExist, paramsValidator } = require('./validateFunctions');
let errorObject = new Object();
const studentModel = models.student;

const studentExist = async (data) => {
    paramsValidator(data);
    await resourceExist(data, studentModel);
};
const studentNotExist = async (data) => {
    const { dni } = data.body;
    const student = await studentModel.findOne({
        where: { dni }
    });
    if (student) {
        errorObject.message = `${dni} already exist`
        errorObject.status = 409
        throw new ExistError(errorObject)
    }
};


module.exports = { studentExist, studentNotExist }