const models = require('../models');
// const ExistError = require('../errors/ExistError');
const { resourceExist, paramsValidator } = require('./validateFunctions');
// let errorObject = new Object();
const groupModel = models.group;

const groupExist = async (data) => {
    paramsValidator(data);
    await resourceExist(data, groupModel);
};
// const groupNotExist = async (data) => {
//     const { description } = data.body;
//     const group = await groupModel.findOne({
//         where: { description }
//     });
//     if (group) {
//         errorObject.message = `${description} already exist`
//         errorObject.status = 409
//         throw new ExistError(errorObject)
//     }
// };


module.exports = { groupExist }