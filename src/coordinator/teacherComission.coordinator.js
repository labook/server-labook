const models = require('../models');
const { resourceExist, paramsValidator } = require('./validateFunctions');

const teacherComission = models.teacher_comission;

const teacherComissionExist = async (data) => {
    paramsValidator(data);
    await resourceExist(data, teacherComission);
};


module.exports = { teacherComissionExist }