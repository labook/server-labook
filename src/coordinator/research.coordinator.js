const models = require('../models');
const ExistError = require('../errors/ExistError');
const { resourceExist, paramsValidator } = require('./validateFunctions');
let errorObject = new Object();
const researchModel = models.research;

const researchExist = async (data) => {
    paramsValidator(data);
    await resourceExist(data, researchModel);
};
const researchNotExist = async (data) => {
    const { name } = data.body;
    const research = await researchModel.findOne({
        where: { name }
    });
    if (research) {
        errorObject.message = `${name} already exist`
        errorObject.status = 409
        throw new ExistError(errorObject)
    }
};


module.exports = { researchExist, researchNotExist }