const models = require('../models');
const { resourceExist, paramsValidator } = require('./validateFunctions');
const statementModel = models.statement;

const statementExist = async (data) => {
    paramsValidator(data);
    await resourceExist(data, statementModel);
};



module.exports = { statementExist }