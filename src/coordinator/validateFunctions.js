const models = require('../models');
const ExistError = require('../errors/ExistError');
const genericSchemas = require('./yupValidations/generic.yup');
const schemas = require('./yupValidations/yup.schemas');
const yup = require('yup');

let errorObject = new Object()

//Subject
const subjectExist = async (data) => {
    const subjectModel = models.subject;
    paramsValidator(data);
    await resourceExist(data, subjectModel);
};

const subjectNotExist = async (data) => {
    const subjectModel = models.subject;
    await resourceNotExist(data, subjectModel);
};

//Selections
const reagentSelectionExist = async (data) => {
    const reagentSelectionModel = models.reagent_selection;
    paramsValidator(data);
    await resourceExist(data, reagentSelectionModel);
}
const materialSelecionExist = async (data) => {
    const materialSelectionModel = models.material_selection;
    paramsValidator(data);
    await resourceExist(data, materialSelectionModel);
}
const equipmentSelectionExist = async (data) => {
    const equipmentSelectionModel = models.equipment_selection;
    paramsValidator(data);
    await resourceExist(data, equipmentSelectionModel);
}

//Reagents
const reagentExist = async (data) => {
    const reagentModel = models.Reagent;
    paramsValidator(data);
    await resourceExist(data, reagentModel)
}

const reagentCasNotExist = async (data) => {
    const { cas } = data.body
    const reagent = await models.Reagent.findOne({
        where: { cas }
    })
    if (reagent) {
        errorObject.message = `Reagent with CAS: ${cas} already exist`
        errorObject.status = 409
        throw new ExistError(errorObject)
    }
}

//Materials
const materialExist = async (data) => {
    const materialModel = models.Material;
    paramsValidator(data)
    await resourceExist(data, materialModel);
}
const materialNotExist = async (data) => {
    const materialModel = models.Material;
    await resourceNotExist(data, materialModel)
}

//Equipments
const equipmentExist = async (data) => {
    const equipmentModel = models.Equipment;
    paramsValidator(data)
    await resourceExist(data, equipmentModel)
}
const equipmentNotExist = async (data) => {
    const equipmentModel = models.Equipment;
    resourceNotExist(data, equipmentModel);
}

//Generic functions
const resourceExist = async (data, model) => {
    const { id } = data.params
    const resource = await model.findOne({
        attributes: [
            "id"
        ],
        where: { id }
    })
    if (!resource) {
        errorObject.message = `ID: ${id} not exist`
        errorObject.status = 404
        throw new ExistError(errorObject)
    }
}
const resourceNotExist = async (data, model) => {
    const { name } = data.body;
    const resource = await model.findOne({
        where: { name }
    });
    if (resource) {
        errorObject.message = `NAME: ${name} already exist`
        errorObject.status = 409
        throw new ExistError(errorObject)
    }
}
const paramsValidator = (data) => {
    const { id } = data.params
    try {
        const idSchema = yup.number().min(1).integer().required()

        idSchema.validateSync(id)
    } catch (err) {
        errorObject.message = err.message
        errorObject.status = 400
        errorObject.path = err.path
        throw new ExistError(errorObject)
    }
}
const paginationValidator = (data) => {
    const { page_number, page_size } = data.query;

    try {
        const numberSchema = yup.number().min(1).integer().required()
        const sizeSchema = yup.number().min(1).integer().required()

        numberSchema.validateSync(page_number);
        sizeSchema.validateSync(page_size);
    } catch (err) {
        errorObject.message = err.message
        errorObject.status = 404
        errorObject.path = err.path
        throw new ExistError(errorObject)
    }

}

// --- YUP ---
const reagentSchema = (data) => {
    genericSchemas.schemaValidator(schemas.Reagent, data);
}
const materialSchema = (data) => {
    genericSchemas.schemaValidator(schemas.Material, data);
}
const equipmentSchema = (data) => {
    genericSchemas.schemaValidator(schemas.Equipment, data);
}
const reagentSelectionSchema = (data) => {
    genericSchemas.schemaValidator(schemas.ReagentSelection, data);
}
const materialSelectionSchema = (data) => {
    genericSchemas.schemaValidator(schemas.MaterialSelection, data);
}
const equipmentSelectionSchema = (data) => {
    genericSchemas.schemaValidator(schemas.EquipmentSelection, data);
}
const subjectSchema = (data) => {
    genericSchemas.schemaValidator(schemas.Subject, data);
}
const comissionSchema = (data) => {
    genericSchemas.schemaValidator(schemas.Comission, data);
}
const studentSchema = (data) => {
    genericSchemas.schemaValidator(schemas.Student, data);
}
const groupSchema = (data) => {
    genericSchemas.schemaValidator(schemas.Group, data);
}
const studentGroupSchema = (data) => {
    genericSchemas.schemaValidator(schemas.Student_Group, data)
}
const teacherSchema = (data) => {
    genericSchemas.schemaValidator(schemas.Teacher, data);
}
const teacherComissionSchema = (data) => {
    genericSchemas.schemaValidator(schemas.Teacher_Comission, data);
}
const researchSchema = (data) => {
    genericSchemas.schemaValidator(schemas.Research, data);
}
const statementSchema = (data) => {
    genericSchemas.schemaValidator(schemas.Statement, data);
}



module.exports = {
    reagentSchema,
    materialSchema,
    equipmentSchema,
    reagentCasNotExist,
    reagentExist,
    materialExist,
    equipmentExist,
    materialNotExist,
    equipmentNotExist,
    paginationValidator,
    paramsValidator,
    reagentSelectionSchema,
    materialSelectionSchema,
    equipmentSelectionSchema,
    reagentSelectionExist,
    materialSelecionExist,
    equipmentSelectionExist,
    subjectSchema,
    comissionSchema,
    studentSchema,
    groupSchema,
    teacherSchema,
    teacherComissionSchema,
    researchSchema,
    statementSchema,
    subjectExist,
    subjectNotExist,
    resourceExist,
    studentGroupSchema
}
