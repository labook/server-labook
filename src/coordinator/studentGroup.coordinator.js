const models = require('../models');
const { resourceExist, paramsValidator } = require('./validateFunctions');

const studentGroupModel = models.student_group;

const studentGroupExist = async (data) => {
    paramsValidator(data);
    await resourceExist(data, studentGroupModel);
};


module.exports = { studentGroupExist }