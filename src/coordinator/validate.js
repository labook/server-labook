const validate = (validationFunction) => {
    return async (req, res, next) => {

        try {
            await validationFunction(req);
            next()

        } catch (error) {
            next(error)
        }

    }
}

module.exports = { validate }