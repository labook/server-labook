const models = require('../models');
// const ExistError = require('../errors/ExistError');
const { resourceExist, paramsValidator } = require('./validateFunctions');
// let errorObject = new Object();
const teacherModel = models.teacher;

const teacherExist = async (data) => {
    paramsValidator(data);
    await resourceExist(data, teacherModel);
};
// const teacherNotExist = async (data) => {
//     const { dni } = data.body;
//     const teacher = await teacherModel.findOne({
//         where: { dni }
//     });
//     if (teacher) {
//         errorObject.message = `${dni} already exist`
//         errorObject.status = 409
//         throw new ExistError(errorObject)
//     }
// };


module.exports = { teacherExist }