const models = require('../models');
const ExistError = require('../errors/ExistError');
const { resourceExist, paramsValidator } = require('./validateFunctions');
let errorObject = new Object();
const comissionModel = models.comission;

const comissionExist = async (data) => {
    paramsValidator(data);
    await resourceExist(data, comissionModel);
};
const comissionNotExist = async (data) => {
    const { description } = data.body;
    const comission = await comissionModel.findOne({
        where: { description }
    });
    if (comission) {
        errorObject.message = `${description} already exist`
        errorObject.status = 409
        throw new ExistError(errorObject)
    }
};


module.exports = { comissionExist, comissionNotExist }