const { updateResource } = require('../helpers/updateResource');

exports.updateResource = async (req, res) => {
    try {
        const { cant } = req.body;
        const { id, type } = req.params;

        const validTypes = ['reagent', 'equipment', 'material'];

        //Validate existing type
        if (!validTypes.includes(type)) {
            res.status(400).json({
                error: `Sorry,only works with this tables ${validTypes}`
            });
        }

        const updatedResource = await updateResource(id, type, cant);

        res.json({
            resource: updatedResource
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}