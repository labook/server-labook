const models = require('../models');


const getMaterialsSelection = async (req, res) => {
    const selections = await models.material_selection.findAll({
        attributes: [
            "id",
            "id_material_fk",
            "id_group_research_fk",
            'cant'
        ],
        include: [
            { as: 'Material', model: models.Material },
            // { as: 'group', model: models.group }
        ]
    });
    res.send(selections)
}

const getMaterialSelection = async (req, res) => {
    const { id } = req.params;

    const selection = await models.material_selection.findOne({
        where: { id },
        attributes: [
            "id",
            "id_material_fk",
            "id_group_research_fk",
            'cant'
        ],
        include: [
            { as: 'Material', model: models.Material },
            // { as: 'group', model: models.group }
        ]
    });
    res.send(selection)
}

const createMaterialSelection = async (req, res) => {
    const { id_material_fk, id_group_research_fk, cant } = req.body;
    const newSelection = await models.material_selection.create({
        id_material_fk,
        id_group_research_fk,
        cant
    });
    res.status(201).send(newSelection);
};

const updateMaterialSelection = async (req, res) => {
    const { id } = req.params;
    const { id_material_fk, id_group_research_fk, cant } = req.body;

    const selection = await models.material_selection.findOne({
        attributes: [
            "id",
            "id_material_fk",
            "id_group_research_fk",
            'cant'
        ],
        where: { id }
    });
    const updatedSelection = await selection.update({
        id_material_fk,
        id_group_research_fk,
        cant
    });
    res.send(updatedSelection);
}

const removedMaterialSelection = async (req, res) => {
    const { id } = req.params;

    await models.material_selection.destroy({
        where: { id }
    });
    res.send();
}

module.exports = { getMaterialsSelection, createMaterialSelection, getMaterialSelection, updateMaterialSelection, removedMaterialSelection }