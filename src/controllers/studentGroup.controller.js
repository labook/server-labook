const models = require('../models');

const getStudentsGroup = async (req, res) => {
    const { id_group } = req.query;
    const dataWhere = id_group ? { id_group_fk: id_group } : null
    const studentsGroup = await models.student_group.findAll({
        where: dataWhere,
        attributes: [
            "id",
            "id_group_fk"
        ],
        include: [
            {
                as: 'student', model: models.student,
                attributes: [
                    "id",
                    "name",
                    "lastname",
                    "dni"
                ],
            },
            {
                as: 'group', model: models.group,
                attributes: [
                    "id"
                ]
            }
        ]
    });
    res.send(studentsGroup);
};

const getStudentGroup = async (req, res) => {
    const { id } = req.params;
    const studentGroup = await models.student_group.findOne({
        where: { id },
        attributes: [
            "id"
        ],
        include: [
            {
                as: 'student', model: models.student,
                attributes: [
                    "id",
                    "name",
                    "lastname",
                    "dni"
                ],
            },
            {
                as: 'group', model: models.group,
                attributes: [
                    "id"
                ]
            }
        ]
    });
    res.send(studentGroup);
};

const createStudentGroup = async (req, res) => {
    const { id_student_fk, id_group_fk } = req.body;
    const newStudentGroup = await models.student_group.create({
        id_student_fk,
        id_group_fk
    });
    res.status(201).send(newStudentGroup);
};

const updateStudentGroup = async (req, res) => {
    const { id } = req.params;
    const { id_student_fk, id_group_fk } = req.body;
    const studentGroup = await models.student_group.findOne({
        where: { id }
    });
    const updatedStudentGroup = await studentGroup.update({
        id_student_fk,
        id_group_fk
    });
    res.send(updatedStudentGroup);
};

const removeStudentGroup = async (req, res) => {
    const { id } = req.params;
    await models.student_group.destroy({
        where: { id }
    });
    res.send();
};


module.exports = { getStudentsGroup, getStudentGroup, createStudentGroup, updateStudentGroup, removeStudentGroup }