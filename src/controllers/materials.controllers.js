const models = require('../models');

const getMaterials = async (req, res) => {
    const { page_number, page_size } = req.query;
    const materials = await models.Material.findAll({
        order: [
            ["id", "ASC"]
        ],
        offset: ((page_number - 1) * page_size),
        limit: (page_size)
    });
    res.send(materials);
}

const findOneMaterial = async id => {
    const material = await models.Material.findOne({
        where: { id }
    });
    return material
}

const getMaterial = async (req, res) => {
    const id = req.params.id;
    const material = await findOneMaterial(id);
    res.send(material);
}

const createMaterial = async (req, res) => {
    const { name, image, description, cant } = req.body;
    const newMaterial = await models.Material.create({
        name,
        image,
        description,
        cant
    });
    res.status(201).send(newMaterial);
}

const deleteMaterial = async (req, res) => {
    const id = req.params.id;

    const deletedMaterial = await models.Material.destroy({
        where: { id }
    });
    res.send()
}

const updateMaterial = async (req, res) => {
    const id = req.params.id;
    const { name, description, image } = req.body;
    const material = await findOneMaterial(id);
    const updatedMaterial = await material.update({
        name,
        image,
        description,
    });
    res.send(updatedMaterial);
}


module.exports = { getMaterials, getMaterial, createMaterial, deleteMaterial, updateMaterial }