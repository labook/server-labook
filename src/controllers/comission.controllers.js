const { QueryTypes } = require('sequelize');
const { sequelize } = require('../models');
const models = require('../models');

const getComissions = async (req, res) => {

    // Id Teacher
    const { id_teacher } = req.query;
    let comissions;

    if (id_teacher) {

        comissions = await sequelize.query(
            `
            SELECT comissions.id, comissions.description,comissions.id_subject_fk,comissions.start_time,
            comissions.end_time, comissions.days, comissions.start_date, comissions.end_date,
            comissions.active, teacher_comissions.id_teacher_fk, teachers.name, teachers.lastname, teachers.dni

            FROM public.comissions, public.teacher_comissions, public.teachers

            WHERE comissions.id = teacher_comissions.id_comission_fk
            AND teacher_comissions.id_teacher_fk = teachers.id
            AND teacher_comissions.id_teacher_fk = ${id_teacher}
              `,
            { type: QueryTypes.SELECT }
        )

        // comissions = await models.comission.findAll({

        //     attributes: {
        //         include: [
        //             [
        //                 sequelize.literal(`
        //                 SELECT comissions.id, comissions.description,comissions.id_subject_fk,comissions.start_time,
        //                 comissions.end_time, comissions.days, comissions.start_date, comissions.end_date,
        //                 comissions.active, teacher_comissions.id_teacher_fk, teachers.name, teachers.lastname, teachers.dni

        //                 FROM public.comissions, public.teacher_comissions, public.teachers

        //                 WHERE comissions.id = teacher_comissions.id_comission_fk 
        //                 AND teacher_comissions.id_teacher_fk = teachers.id 
        //                 AND teacher_comissions.id_teacher_fk = ${id_teacher}
        //                 `),
        //                 // 'laughReactionsCount'
        //             ]
        //         ]
        //     }
        // })

    } else {
        comissions = await models.comission.findAll({
            attributes: [
                "id",
                "description",
                "start_time",
                "end_time",
                "days",
                "id_subject_fk",
                "start_date",
                "end_date",
                "active",
            ],
            include: [
                { as: 'subject', model: models.subject },
                {
                    as: 'teacher_comissions', model: models.teacher_comission,
                    include: [
                        { as: 'teacher', model: models.teacher }
                    ]
                }
            ]
        });
    }


    res.send(comissions);
};

const getComission = async (req, res) => {
    const { id } = req.params;
    const comission = await models.comission.findOne({
        where: { id },
        // attributes: [
        //     "id",
        //     "description",
        //     "start_time",
        //     "end_time",
        //     "days",
        //     "id_subject_fk",
        //     "start_date",
        //     "end_date",
        //     "active",
        // ],
        include: [
            { as: 'subject', model: models.subject },
            {
                as: 'teacher_comissions', model: models.teacher_comission,
                include: [
                    { as: 'teacher', model: models.teacher }
                ]
            },
            {
                as: 'students', model: models.student
            }
        ]
    });
    res.send(comission);
};

const createComission = async (req, res) => {
    const { description, start_time, end_time, days, id_subject_fk, start_date, end_date, active } = req.body;
    const newComission = await models.comission.create({
        description,
        start_time,
        end_time,
        days,
        id_subject_fk,
        start_date,
        end_date,
        active,
    });
    res.status(201).send(newComission);
};

const updateComission = async (req, res) => {
    const { id } = req.params;
    const { description, start_time, end_time, days, id_subject_fk, start_date, end_date, active } = req.body;
    const comission = await models.comission.findOne({
        where: { id }
    });
    const updatedComission = await comission.update({
        description,
        start_time,
        end_time,
        days,
        id_subject_fk,
        start_date,
        end_date,
        active,
    });
    res.send(updatedComission);
}

const removeComission = async (req, res) => {
    const { id } = req.params
    await models.comission.destroy({
        where: { id }
    });
    res.send();
};

module.exports = { getComissions, getComission, createComission, updateComission, removeComission }