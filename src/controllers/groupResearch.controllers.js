const models = require('../models');

exports.getGroupResearch = async (req, res) => {
    //Id research
    const { id_group } = req.query;
    const dataWhere = id_group ? { id_group_fk : id_group} : null

    const groupResearch = await models.group_research.findAll({
        where: dataWhere,
        include: [
            { as: 'step_researches', model: models.step_research },
            {
                as: 'equipment_selections', model: models.equipment_selection,
                attributes: ['id','cant'],
                include: [
                    {
                        as: 'Equipment', model: models.Equipment
                    }
                ]
            },
            {
                as: 'material_selections', model: models.material_selection,
                attributes: ["id",'cant'],
                include: [
                    {
                        as: 'Material', model: models.Material
                    }
                ]
            },
            {
                as: 'reagent_selections', model: models.reagent_selection,
                attributes: ['id','cant'],
                include: [
                    {
                        as: 'Reagent', model: models.Reagent
                    }
                ]
            },
        ]
    });

    res.json({
        groupResearch
    });

}
exports.getGroupResearchById = async (req, res) => {
    //Id research
    const { id } = req.params;

    const groupResearch = await models.group_research.findOne({
        where: {id},
        include: [
            { as: 'step_researches', model: models.step_research },
            {
                as: 'equipment_selections', model: models.equipment_selection,
                attributes: ['id', 'cant'],
                include: [
                    {
                        as: 'Equipment', model: models.Equipment
                    }
                ]
            },
            {
                as: 'material_selections', model: models.material_selection,
                attributes: ["id", 'cant'],
                include: [
                    {
                        as: 'Material', model: models.Material
                    }
                ]
            },
            {
                as: 'reagent_selections', model: models.reagent_selection,
                attributes: ['id','cant'],
                include: [
                    {
                        as: 'Reagent', model: models.Reagent
                    }
                ]
            },
        ]
    });

    res.json({
        groupResearch
    });

}


exports.createGroupResearch = async (req, res) => {

    const { id_group_fk, id_research_fk } = req.body;

    const newGroupResearch = await models.group_research.create({
        id_group_fk,
        id_research_fk
    });

    res.status(201).json({
        newGroupResearch
    });
}