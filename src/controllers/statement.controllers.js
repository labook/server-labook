const models = require('../models');
const fs = require('fs');

const getStatements = async (req, res) => {
    const { id_research } = req.query;
    const dataWhere = id_research ? { id_research_fk: id_research } : null
    const statements = await models.statement.findAll({
        where: dataWhere
    });
    res.send(statements);
};

const getStatement = async (req, res) => {
    const { id } = req.params;
    const statement = await models.statement.findOne({
        where: { id }
    });
    res.send(statement);
};

const createStatement = async (req, res) => {
    const pdf = req.file
    const { id_research_fk } = req.body;
    
    const newStatement = await models.statement.create({
        statement_pdf: `http://localhost:8000/uploads/${pdf.filename}`,
        id_research_fk
    });
    res.status(201).send(newStatement);
};

const updateStatement = async (req, res) => {
    const { id } = req.params;
    const pdf = req.file
    const { id_research_fk } = req.body;

    const statement = await models.statement.findOne({
        where: { id }
    });

    const updatedStatement = await statement.update({
        statement_pdf: `http://localhost:8000/uploads/${pdf.filename}`,
        id_research_fk
    });
    res.send(updatedStatement);
};

const removeStatement = async (req, res) => {
    const { id } = req.params;
    // await models.statement.destroy({
    //     where: { id }
    // });

    await models.statement.destroy({
        where: {id_research_fk: id}
    })
    res.send();
};

module.exports = { getStatements, getStatement, createStatement, updateStatement, removeStatement }