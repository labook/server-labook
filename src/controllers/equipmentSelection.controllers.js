const models = require('../models');

const getEquipmentSelections = async (req, res) => {
    const selections = await models.equipment_selection.findAll({
        attributes: [
            "id",
            "id_equipment_fk",
            'cant'
        ],
        include: [
            { as: 'Equipment', model: models.Equipment },
            // { as: 'group', model: models.group }
        ]
    });
    res.send(selections);
};

const getEquipmentSelection = async (req, res) => {
    const { id } = req.params;
    const selection = await models.equipment_selection.findOne({
        where: { id },
        attributes: [
            "id",
            "id_equipment_fk",
            "id_group_research_fk",
            'cant'
        ],
        include: [
            { as: 'Equipment', model: models.Equipment },
            // { as: 'group', model: models.group }
        ]
    });
    res.send(selection);
};

const createEquipmentSelection = async (req, res) => {
    const { id_equipment_fk, id_group_research_fk, cant } = req.body;

    const newSelection = await models.equipment_selection.create({
        id_equipment_fk,
        id_group_research_fk,
        cant
    });
    res.send(newSelection);
};

const updateEquipmentSelection = async (req, res) => {
    const { id } = req.params;
    const { id_equipment_fk, id_group_research_fk, cant } = req.body;

    const selection = await models.equipment_selection.findOne({
        where: { id },
        attributes: [
            "id",
            "id_equipment_fk",
            "id_group_research_fk",
            'cant'
        ]
    });
    const updatedSelection = await selection.update({
        id_equipment_fk,
        id_group_research_fk,
        cant
    });
    res.send(updatedSelection);
};

const removeEquipmentSelection = async (req, res) => {
    const { id } = req.params;

    await models.equipment_selection.destroy({
        where: { id }
    });
    res.send();
};

module.exports = { getEquipmentSelections, getEquipmentSelection, createEquipmentSelection, updateEquipmentSelection, removeEquipmentSelection }