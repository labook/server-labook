const models = require('../models');

exports.getSteps = async (req, res) => {
    const steps = await models.step_research.findAll();
    res.send(steps);
}

exports.getStep = async (req, res) => {
    const { id } = req.params;

    const step = await models.step_research.findOne({
        where: { id }
    });
    res.send(step);
}

exports.createStep = async (req, res) => {
    const { step_number, step_description, teacher_check, id_group_research_fk } = req.body;
    
    const newStep = await models.step_research.create({
        step_number,
        step_description,
        teacher_check,
        id_group_research_fk,
    });

    res.status(201).send(newStep);
}

exports.updateStep = async (req, res) => {
    const { id } = req.params;
    const { step_number, step_description, teacher_check, id_group_research_fk } = req.body;

    const step = await models.step_research.findOne({
        where: { id }
    });
    const updatedStep = await step.update({
        step_number,
        step_description,
        teacher_check,
        id_group_research_fk
    });
    res.send(updatedStep);
}

exports.deleteStep = async (req, res) => {
    const { id } = req.params;

    await models.step_research.destroy({
        where: { id }
    });
    res.send()

}