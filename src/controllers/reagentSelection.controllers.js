const models = require('../models');


const getReagentsSelection = async (req, res) => {
    const selections = await models.reagent_selection.findAll({
        attributes: [
            'id',
            'id_reagent_fk',
            'id_group_research_fk,',
            'cant'
        ],
        include: [
            { as: 'Reagent', model: models.Reagent },
            // { as: 'group', model: models.group }
        ]
    });
    res.send(selections);
}

const getReagentSelection = async (req, res) => {
    const { id } = req.params;

    const selection = await models.reagent_selection.findOne({
        where: { id },
        attributes: [
            'id',
            'id_reagent_fk',
            'id_group_research_fk,',
            'cant'
        ],
        include: [
            { as: 'Reagent', model: models.Reagent },
            // { as: 'group', model: models.group }
        ]
    });
    res.send(selection);
}

const createReagentSelection = async (req, res) => {
    const { id_reagent_fk, id_group_research_fk, cant } = req.body;

    const newSelection = await models.reagent_selection.create({
        id_reagent_fk,
        id_group_research_fk,
        cant
    });
    res.status(201).send(newSelection)
}

const updateReagentSelection = async (req, res) => {
    const { id } = req.params;
    const { id_reagent_fk, id_group_research_fk, cant } = req.body;

    const selection = await models.reagent_selection.findOne({
        where: { id },
        attributes: [
            'id',
            'id_reagent_fk',
            'id_group_research_fk,',
            'cant'
        ]
    });
    const updatedSelection = await selection.update({
        id_reagent_fk,
        id_group_research_fk,
        cant
    });
    res.send(updatedSelection)
}

const removeReagentSelection = async (req, res) => {
    const { id } = req.params;

    await models.reagent_selection.destroy({
        where: { id }
    });
    res.send()
}

module.exports = { getReagentsSelection, getReagentSelection, createReagentSelection, updateReagentSelection, removeReagentSelection }