const { QueryTypes } = require('sequelize');
const { sequelize } = require('../models');
const models = require('../models');

/*
SELECT research.id as researchId, research.name as researchName, 
    students.id as studentId, groups.id as groupId
    FROM public.research, 
    public.group_researches, 
    public.groups, 
    public.student_groups, 
    public.students
    WHERE group_researches.id_research_fk = research.id
    AND groups.id = group_researches.id_group_fk
    AND student_groups.id_group_fk = groups.id
    AND students.id = student_groups.id_student_fk 
*/


const getResearchs = async (req, res) => {

    // Id student
    const { id } = req.query;
    let researchs;

    if(id){

        researchs = await sequelize.query(
            `
            SELECT research.id as research_id, research.name as research_name, research.deliver_date, research.description,
            students.id as student_id, groups.id as group_id, groups.name as group_name, group_researches.id as group_research_id
            
            FROM research, group_researches, groups, student_groups, students
            
            WHERE group_researches.id_research_fk = research.id
            AND groups.id = group_researches.id_group_fk
            AND student_groups.id_group_fk = groups.id
            AND students.id = student_groups.id_student_fk
            AND students.id = ${id} 
            `,
            { type: QueryTypes.SELECT }
        )

    }else {
        researchs = await models.research.findAll();
    }


    res.send(researchs);
};

const getResearch = async (req, res) => {
    const { id } = req.params;
    const research = await models.research.findOne({
        attributes: [
            "id",
            "name",
            "deliver_date",
            "description"
        ],
        where: { id },
        include: [
            // {
            //     as: "step_researches", model: models.step_research
            // },
            {
                as: 'statements', model: models.statement,
                attributes: [
                    "id",
                    "statement_pdf"
                ]
            },
            //  {
            //     as: 'groups', model: models.group
            // }
        ]
    });
    res.send(research);
};

const createResearch = async (req, res) => {
    const { name, deliver_date, description } = req.body;
    const newResearch = await models.research.create({
        name,
        deliver_date,
        description
    });
    res.status(201).send(newResearch);
};

const updateResearch = async (req, res) => {
    const { id } = req.params;
    const { name, deliver_date, description } = req.body;
    const research = await models.research.findOne({
        where: { id }
    });
    const updatedResearch = await research.update({
        name,
        deliver_date,
        description
    });
    res.send(updatedResearch);
};

const removeResearch = async (req, res) => {
    const { id } = req.params;
    await models.research.destroy({
        where: { id }
    });
    res.send();
};

module.exports = { getResearchs, getResearch, createResearch, updateResearch, removeResearch }