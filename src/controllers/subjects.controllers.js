const models = require('../models');

const getSubjects = async (req, res) => {
    const subjects = await models.subject.findAll({
        attributes: [
            "id",
            "name"
        ],
        include: [
            { as: 'comissions', model: models.comission }
        ]
    });
    res.send(subjects);
};

const getSubject = async (req, res) => {
    const { id } = req.params;
    const subject = await models.subject.findOne({
        attributes: [
            "id",
            "name"
        ],
        where: { id },
        include: [
            { as: 'comissions', model: models.comission }
        ]
    });
    res.send(subject)
};

const createSubject = async (req, res) => {
    const { name } = req.body;
    const newSubject = await models.subject.create({
        name,
    });
    res.status(201).send(newSubject);
};

const updateSubject = async (req, res) => {
    const { name } = req.body;
    const { id } = req.params;

    const subject = await models.subject.findOne({
        where: { id }
    });
    const updatedSubject = await subject.update({
        name,
    });
    res.send(updatedSubject);
};

const removeSubject = async (req, res) => {
    const { id } = req.params;
    await models.subject.destroy({
        where: { id }
    });
    res.send()
};


module.exports = { getSubjects, getSubject, createSubject, updateSubject, removeSubject }