const models = require('../models');

const getTeachersComission = async (req, res) => {
    const teachersComission = await models.teacher_comission.findAll({
        include: [
            { as: 'research', model: models.research }
        ]
    });
    res.send(teachersComission);
};

const getTeacherComission = async (req, res) => {
    const { id } = req.params;
    const teacherComission = await models.teacher_comission.findOne({
        where: { id },
        include: [
            { as: 'research', model: models.research }
        ]
    });
    res.send(teacherComission);
};

const createTeacherComission = async (req, res) => {
    const { id_teacher_fk, id_comission_fk, is_holder, id_research_fk } = req.body;
    const newTeacherComission = await models.teacher_comission.create({
        id_teacher_fk,
        id_comission_fk,
        is_holder,
        id_research_fk
    });
    res.status(201).send(newTeacherComission);
};

const updateTeacherComission = async (req, res) => {
    const { id } = req.params;
    const { id_teacher_fk, id_comission_fk, id_research_fk } = req.body;
    const teacherComission = await models.teacher_comission.findOne({
        where: { id }
    });
    const updatedTeacherComission = await teacherComission.update({
        id_teacher_fk,
        id_comission_fk,
        is_holder,
        id_research_fk
    });
    res.send(updatedTeacherComission);
};

const removeTeacherComission = async (req, res) => {
    const { id } = req.params;
    await models.teacher_comission.destroy({
        where: { id }
    });
    res.send();
};


module.exports = { getTeachersComission, getTeacherComission, createTeacherComission, updateTeacherComission, removeTeacherComission }