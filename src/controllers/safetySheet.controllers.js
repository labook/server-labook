const models = require('../models');

const getSafetySheets = async (req, res) => {
    const { page_number, page_size } = req.query;
    const safetySheets = await models.safety_sheets.findAll({
        order: [
            ["id", "ASC"]
        ],
        offset: ((page_number - 1) * page_size),
        limit: (page_size)
    });
    res.send(safetySheets);
}

const findOne = async (id) => {
    return await models.safety_sheets.findOne({
        where: { id },
        include: [
            { as: 'Reagent', model: models.Reagent }
        ]
    })
}

const getSafetySheet = async (req, res) => {
    const { id } = req.params;
    const safetySheet = await findOne(id)
    res.send(safetySheet);
}

const createSafetySheet = async (req, res) => {
    const pdf = req.file;
    const { id_reagent_fk } = req.body;

    const newSafetySheet = await models.safety_sheets.create({
        safety_pdf: `http://localhost:8000/uploads/${pdf.filename}`,
        id_reagent_fk
    });
    res.status(201).send(newSafetySheet);
}

const updateSafetySheet = async (req, res) => {
    const pdf = req.file;
    const { id_reagent_fk } = req.body;
    const { id } = req.params;

    const safetySheet = await models.safety_sheets.findOne({
        where: { id }
    });
    const updatedSafetySheet = await safetySheet.update({
        safety_pdf: `http://localhost:8000/uploads/${pdf.filename}`,
        id_reagent_fk
    });

    res.send(updatedSafetySheet)
}

const deleteSafetySheet = async (req, res) => {
    const { id } = req.params
    await models.safety_sheets.destroy({
        where: { id }
    })
    res.send()
}

module.exports = { getSafetySheets, getSafetySheet, createSafetySheet, updateSafetySheet, deleteSafetySheet }