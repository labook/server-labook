const models = require('../models');


const getGroups = async (req, res) => {
    const { id_comission } = req.query;
    const dataWhere = id_comission ? { id_comission_fk: id_comission } : null
    const groups = await models.group.findAll({
        where: dataWhere,
        include: [
            {
                as: 'student_groups', model: models.student_group,
                attributes: [
                    "id",
                ],
                include: [
                    {
                        as: 'student', model: models.student,
                        attributes: [
                            "id",
                            "name",
                            "lastname",
                            "dni"
                        ]
                    }
                ]
            },
            {
                as: 'group_researches', model: models.group_research
            }

        ]
    });

    res.send(groups)
}

const getGroup = async (req, res) => {
    const { id } = req.params;
    const group = await models.group.findOne({
        where: { id },
        attributes: [
            "id",
            "name"
        ],
        include: [
            {
                as: 'comission', model: models.comission,
                attributes: [
                    "id",
                    "description",
                    "start_time",
                    "end_time",
                    "days",
                    "id_subject_fk",
                    "start_date",
                    "end_date",
                    "active",
                ]
            },
            // {
            //     as: 'research', model: models.research,
            //     attributes: [
            //         "id",
            //         "name",
            //         "deliver_date"
            //     ],
            //     include: [
            //         {
            //             as: 'statements', model: models.statement
            //         }
            //     ]
            // },
            {
                as: 'student_groups', model: models.student_group,
                attributes: [
                    "id",
                ],
                include: [
                    {
                        as: 'student', model: models.student,
                        attributes: [
                            "id",
                            "name",
                            "lastname",
                            "dni"
                        ]
                    }
                ]
            },
            // {
            //     as: 'material_selections', model: models.material_selection,
            //     attributes: ["id"],
            //     include: [
            //         {
            //             as: 'Material', model: models.Material
            //         }
            //     ]
            // },
            // {
            //     as: 'reagent_selections', model: models.reagent_selection,
            //     attributes: ['id'],
            //     include: [
            //         {
            //             as: 'Reagent', model: models.Reagent
            //         }
            //     ]
            // },
            // {
            //     as: 'equipment_selections', model: models.equipment_selection,
            //     attributes: ['id'],
            //     include: [
            //         {
            //             as: 'Equipment', model: models.Equipment
            //         }
            //     ]
            // }
        ]
    });
    res.send(group);
}

const createGroup = async (req, res) => {
    //Anadir atributos a la entidad grupo, aunque sea genericos
    const { id_comission_fk, name } = req.body;
    const group = await models.group.create({
        // id_research_fk,
        id_comission_fk,
        name
    });
    res.status(201).send(group)
}

const updateGroup = async (req, res) => {
    const { id } = req.params;
    const { id_comission_fk, name } = req.body;
    const group = await models.group.findOne({
        where: { id }
    });
    const updatedGroup = await group.update({
        // id_research_fk,
        id_comission_fk,
        name
    });
    res.send(updatedGroup);
};

const removeGroup = async (req, res) => {
    const { id } = req.params;

    await models.group.destroy({
        where: { id }
    });
    res.send();
}

module.exports = { getGroups, createGroup, getGroup, updateGroup, removeGroup }
