const models = require('../models');

const getStatusStudent = async (req, res) => {
    const { id_comission } = req.query;

    const dataWhere = id_comission ? { id_comission_fk: id_comission } : null;
    const students = await models.student.findAll({
        where: dataWhere
    });
    
    res.send(students)

    // const studentGroups = await Promise.all(students.map(async student => {
    //     var active = false;
    //     const studentGroup = await models.student_group.findAll({
    //         where: { id_student_fk: student.id }
    //     })
    //     studentGroup.length === 0 ? active : active = true

    //     return { student, status: active }
    // }));

    // res.send(studentGroups);
}

const getStudents = async (req, res) => {
    const { id_comission } = req.query;

    //Filtro de estudiantes por comision
    const dataWhere = id_comission ? { id_comission_fk: id_comission } : null;
    const students = await models.student.findAll({
        where: dataWhere,
        // attributes: [
        //     "id",
        //     "name",
        //     "lastname",
        //     "id_comission_fk",
        //     "dni"
        // ],
        // include: {
        //     as: 'student_groups', model: models.student_group
        // }
    });
    res.send(students);
};

const getStudent = async (req, res) => {
    const { id } = req.params;
    const student = await models.student.findOne({
        where: { id }
    });
    res.send(student);
};

const createStudent = async (req, res) => {
    const { name, lastname, dni, id_comission_fk, id_group_fk } = req.body;
    const newStudent = await models.student.create({
        name,
        lastname,
        dni,
        id_comission_fk,
        id_group_fk
    });
    res.status(201).send(newStudent);
};

const updateStudent = async (req, res) => {
    const { id } = req.params;
    const { name, lastname, dni, id_comission_fk, id_group_fk } = req.body;
    const student = await models.student.findOne({
        where: { id }
    });
    const updatedStudent = await student.update({
        name,
        lastname,
        dni,
        id_comission_fk,
        id_group_fk
    });
    res.send(updatedStudent)
};

const removeStudent = async (req, res) => {
    const { id } = req.params;
    await models.student.destroy({
        where: { id }
    });
};

module.exports = { getStudents, getStudent, createStudent, updateStudent, removeStudent, getStatusStudent }