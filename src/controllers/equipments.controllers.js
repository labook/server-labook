const models = require('../models');

const getEquipments = async (req, res) => {
    const { page_number, page_size } = req.query;
    const equipments = await models.Equipment.findAll({
        order: [
            ["id", "ASC"]
        ],
        offset: ((page_number - 1) * page_size),
        limit: (page_size)
    });
    res.send(equipments);
}

const findOneEquipment = async id => {
    const equipment = await models.Equipment.findOne({
        where: { id }
    });
    return equipment
}

const getEquipment = async (req, res) => {
    const id = req.params.id;
    const equipment = await findOneEquipment(id)
    res.send(equipment);
}

const createEquipment = async (req, res, next) => {
    const { name, image, description, cant } = req.body;

    const newEquipment = await models.Equipment.create({
        name,
        image,
        description,
        cant
    });
    res.status(201).send(newEquipment);
}

const deleteEquipment = async (req, res) => {
    const id = req.params.id;

    const deletedEquipment = await models.Equipment.destroy({
        where: { id }
    });
    res.send();
}

const updateEquipment = async (req, res) => {
    const id = req.params.id;
    const { name, description, image } = req.body;
    const equipment = await findOneEquipment(id)
    const equipmentUpdated = await equipment.update({
        name,
        image,
        description
    });
    res.send(equipmentUpdated);
}


module.exports = { getEquipments, getEquipment, createEquipment, updateEquipment, deleteEquipment }