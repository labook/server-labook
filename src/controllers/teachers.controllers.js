const models = require('../models');


const getTeachers = async (req, res) => {
    const teachers = await models.teacher.findAll({
        attributes: [
            "id",
            "name",
            "lastname",
            "dni"
        ]
    });
    res.send(teachers);
};

const getTeacher = async (req, res) => {
    const { id } = req.params;
    const teacher = await models.teacher.findOne({
        where: { id },
        attributes: [
            "id",
            "name",
            "lastname",
            "dni"
        ]
    });
    res.send(teacher);
};

const createTeacher = async (req, res) => {
    const { name, lastname, dni } = req.body;
    const newTeacher = await models.teacher.create({
        name,
        lastname,
        dni
    });
    res.status(201).send(newTeacher);
};

const updateTeacher = async (req, res) => {
    const { id } = req.params;
    const { name, lastname, dni } = req.body;

    const teacher = await models.teacher.findOne({
        where: { id }
    });
    const updatedTeacher = await teacher.update({
        name,
        lastname,
        dni
    });
    res.send(updatedTeacher);
};

const removeTeacher = async (req, res) => {
    const { id } = req.params;
    await models.teacher.destroy({
        where: { id }
    });
    res.send();
};

module.exports = { getTeachers, getTeacher, createTeacher, updateTeacher, removeTeacher }