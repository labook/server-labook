const models = require('../models');
// const fs = require('fs')

const getReagents = async (req, res) => {
    const { page_number, page_size } = req.query;
    const reagents = await models.Reagent.findAll({
        order: [
            ["id", "ASC"]
        ],
        offset: ((page_number - 1) * page_size),
        limit: (page_size)
    })
    res.send(reagents);
}

const findOneReagent = async id => {
    const reagent = await models.Reagent.findOne({
        where: { id }
    });
    return reagent
}

const getReagent = async (req, res) => {
    const { id } = req.params;
    const reagent = await findOneReagent(id);
    res.send(reagent);
}

const createReagent = async (req, res) => {
    const { name, cas, description, image, cant } = req.body;
    const newReagent = await models.Reagent.create({
        name,
        cas,
        image,
        description,
        cant
    });
    res.status(201).send(newReagent);
}


const updateReagent = async (req, res) => {
    const { id } = req.params;
    const { name, cas, description, image } = req.body;
    const reagent = await findOneReagent(id)
    const updatedReagent = await reagent.update({
        name,
        cas,
        image,
        description,
    });

    res.send(updatedReagent)
}


const deleteReagent = async (req, res) => {
    const { id } = req.params;
    await models.Reagent.destroy({
        where: { id }
    })
    res.send();
}

module.exports = { getReagents, getReagent, createReagent, updateReagent, deleteReagent }