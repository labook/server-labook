const models = require('../../models');


exports.getRols = async (req, res) => {
    const rols = await models.rol.findAll();
    res.send(rols);
}

exports.getRol = async (req, res) => {

    const { id } = req.params;

    const rol = await models.rol.findOne({
        where: { id },
        include: [
            { as: 'users', model: models.user }
        ]
    });
    res.send(rol);
}

exports.createRol = async (req, res) => {

    const { rol_name } = req.body;
    const newRol = await models.rol.create({
        rol_name
    });
    res.status(201).send(newRol);
}

exports.updateRol = async (req, res) => {

    const { id } = req.params;
    const { rol_name} = req.body;

    const rol = await models.rol.findOne({
        where: { id }
    });
    const updatedRol = await rol.update({
        rol_name
    });
    res.send(updatedRol)
}

exports.removeRol = async (req, res) => {

    const { id } = req.params;
    await models.rol.destroy({
        where: { id }
    });
    res.send();
}