const models = require('../../models');
const bcryptjs = require('bcryptjs');
const { createJwt } = require('../../helpers/jwt');

exports.adminSignUp = async (req, res) => {
    const { email, password, dni } = req.body;

    const rolAdmin = await models.rol.findOne({
        where: { rol_name: 'admin' }
    });


    //Valido si el mail o el dni ya fueron usados para otro usuario
    const [userDB_email, userDB_dni] = await Promise.all([
        models.user.findOne({
            where: { email }
        }),
        models.user.findOne({
            where: { dni }
        }),
    ]);

    if (userDB_email) {
        return res.status(403).json({ error: 'El email ya esta en uso' });
    }
    if (userDB_dni) {
        return res.status(403).json({
            error: 'Ya fue creada una cuenta con ese dni'
        });
    }

    // Encrypt password
    const salt = bcryptjs.genSaltSync(); //Data aleatoria
    const cryptPass = bcryptjs.hashSync(password, salt);


    let newUser = await models.user.create({
        id_rol_fk: rolAdmin.id,
        email,
        password: cryptPass,
        dni,
        id_student_fk: null,
        id_teacher_fk: null
    });

    const token = await createJwt(newUser.id);

    newUser = newUser.toJSON();
    res.json(
        {
            ...newUser,
            token
        }
    );
}

exports.signUp = async (req, res) => {

    const { id_rol_fk, email, password, dni } = req.body;

    try {

        let newUser;

        //Valido si el mail o el dni ya fueron usados para otro usuario
        const [userDB_email, userDB_dni] = await Promise.all([
            models.user.findOne({
                where: { email }
            }),
            models.user.findOne({
                where: { dni }
            }),
        ]);

        if (userDB_email) {
            return res.status(403).json({ error: 'El email ya esta en uso' });
        }
        if (userDB_dni) {
            return res.status(403).json({
                error: 'Ya fue creada una cuenta con ese dni'
            });
        }

        const [studentDB, teacherDB, rolsDB] = await Promise.all([
            models.student.findOne({
                where: { dni }
            }),
            models.teacher.findOne({
                where: { dni }
            }),
            models.rol.findAll()
        ]);

        // Encrypt password
        const salt = bcryptjs.genSaltSync(); //Data aleatoria
        const cryptPass = bcryptjs.hashSync(password, salt);


        if (studentDB) {
            // Cambiar a find
            let studentRol = rolsDB.filter(r => r.rol_name === 'student' && r.id);
            newUser = await models.user.create({
                id_rol_fk: studentRol[0].id,
                dni,
                email,
                password: cryptPass,
                id_student_fk: studentDB.id,
                id_teacher_fk: null
            });
        } else {
            let teacherRol = rolsDB.filter(r => r.rol_name === 'teacher' && r.id);
            newUser = await models.user.create({
                id_rol_fk: teacherRol[0].id,
                dni,
                email,
                password: cryptPass,
                id_student_fk: null,
                id_teacher_fk: teacherDB.id
            })
        }

        const token = await createJwt(newUser.id);

        newUser = newUser.toJSON();
        res.json(
            {
                ...newUser,
                token
            }
        );


    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Internal Server Error'
        });
    }

}

exports.signIn = async (req, res) => {

    const { dni, password } = req.body;

    try {

        let userDB = await models.user.findOne({
            where: { dni },
            include: [
                { as: 'rol', model: models.rol }
            ]
        });

        if (!userDB) {
            return res.status(401).json({
                error: 'El usuario no existe'
            });
        }

        //Verificar password
        const validPassowrd = bcryptjs.compareSync(password, userDB.password);

        if (!validPassowrd) {
            return res.status(400).json({
                error: 'La contraseña no es correcta'
            });
        }
        const token = await createJwt(userDB.id);


        userDB = userDB.toJSON();
        res.json(
            {
                ...userDB,
                token
            }
        );
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Internal Server Error'
        });
    }
}



