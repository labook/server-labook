'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('reagent_selections', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_reagent_fk: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'Reagents',
          key: 'id'
        }
      },
      id_group_research_fk: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'group_researches',
          key: 'id'
        }
      },
      cant: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      // id_group_fk: {
      //   allowNull: false,
      //   type: Sequelize.INTEGER,
      //   references: {
      //     model: 'groups',
      //     key: 'id'
      //   }
      // },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }, {
      tableName: 'reagent_selections',
      freezeTableName: true
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('reagent_selections');
  }
};
