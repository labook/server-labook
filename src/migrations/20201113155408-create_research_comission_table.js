'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('research_comissions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      // is_holder: {
      //   type: Sequelize.BOOLEAN
      // },
      id_research_fk: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'research',
          key: 'id'
        }
      },
      id_comission_fk: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'comissions',
          key: 'id'
        }
      },
      // id_research_fk: {
      //   allowNull: true,
      //   type: Sequelize.INTEGER,
      //   references: {
      //     model: 'research',
      //     key: 'id'
      //   }
      // },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }, {
      tableName: 'research_comissions',
      freezeTableName: true
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('research_comissions')
  }
};
