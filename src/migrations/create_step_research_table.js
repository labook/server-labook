'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('step_researches', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      step_number: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      step_description: {
        allowNull: false,
        type: Sequelize.STRING
      },
      teacher_check: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },
      id_group_research_fk: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'group_researches',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    },
    {
      tableName: 'step_researches',
      freezeTableName: true
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('step_researches')
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
