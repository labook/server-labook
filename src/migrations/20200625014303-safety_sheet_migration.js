'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('safety_sheets', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            safety_pdf: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        },{
            tableName: 'safety_sheets',
            freezeTableName: true
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('safety_sheets');
    }
};