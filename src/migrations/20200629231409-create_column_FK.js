'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
      return queryInterface.addColumn(
        'safety_sheets', // name of Source model
        'id_reagent_fk', // name of the key we're adding 
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'Reagents', // name of Target model
            key: 'id', // key in Target model that we're referencing
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        }
      );
    },
  
    down: (queryInterface, Sequelize) => {
      return queryInterface.removeColumn(
        'safety_sheets', // name of Source model
        'id_reagent_fk' // key we want to remove
      );
    }
  };