'use strict';
module.exports = (sequelize, DataTypes) => {
    const subject = sequelize.define('subject', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: DataTypes.STRING,
    }, {});
    subject.associate = function (models) {
        // associations can be defined here
        subject.hasMany(models.comission, {
            //alias: 'comission',
            foreignKey: 'id_subject_fk',
            sourceKey: 'id'
        })
    };
    return subject;
};