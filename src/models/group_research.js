'use strict';
module.exports = (sequelize, DataTypes) => {
    const group_research = sequelize.define('group_research', {
        id: {
            primaryKey: true,
            autoIncrement: true,
            type: DataTypes.INTEGER
        },
        id_group_fk: {
            type: DataTypes.INTEGER,
            foreignKey: true
        },
        id_research_fk: {
            type: DataTypes.INTEGER,
            foreignKey: true
        }
    }, {});
    group_research.associate = function (models) {
        group_research.hasOne(models.group, {
            foreignKey: 'id',
            sourceKey: 'id_group_fk'
        });
        group_research.hasOne(models.research, {
            foreignKey: 'id',
            sourceKey: 'id_research_fk'
        });
        group_research.hasMany(models.step_research, {
            foreignKey: 'id_group_research_fk',
            sourceKey: 'id'
        });
        group_research.hasMany(models.material_selection, {
            foreignKey: 'id_group_research_fk',
            sourceKey: 'id'
        });
        group_research.hasMany(models.reagent_selection, {
            foreignKey: 'id_group_research_fk',
            sourceKey: 'id'
        });
        group_research.hasMany(models.equipment_selection, {
            foreignKey: 'id_group_research_fk',
            sourceKey: 'id',
        });
    };
    return group_research;
};