'use strict'

module.exports = (sequelize, DataTypes) => {
    const teacher = sequelize.define('teacher', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: DataTypes.STRING,
        lastname: DataTypes.STRING,
        dni: DataTypes.INTEGER,
    }, {});
    teacher.associate = function (models) {
        teacher.hasMany(models.teacher_comission, {
            foreignKey: 'id_teacher_fk',
            sourceKey: 'id'
        });
        teacher.hasMany(models.user, {
            foreignKey: 'id_teacher_fk',
            sourceKey: 'id'
        });
    };
    return teacher;
};