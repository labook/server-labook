'use strict';
module.exports = (sequelize, DataTypes) => {
    const step_research = sequelize.define('step_research', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        step_number: {
            type: DataTypes.INTEGER
        },
        step_description: {
            type: DataTypes.STRING
        },
        teacher_check: {
            type: DataTypes.BOOLEAN
        },
        id_group_research_fk: {
            type: DataTypes.INTEGER,
            foreignKey: true
        }
    }, {});
    step_research.associate = function (models) {
        // associations can be defined here
        step_research.hasOne(models.group_research, {
            foreignKey: 'id',
            sourceKey: 'id_group_research_fk'
        })
       

    };
    return step_research;
};

// step_number: integer,
//   step_name: string,
//   step_description: string,
//   teacher_check: boolean,
//   id_research_fk: id_Research