'use strict';
module.exports = (sequelize, DataTypes) => {
    const group = sequelize.define('group', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING
        },
        // id_research_fk: {
        //     type: DataTypes.INTEGER,
        //     foreignKey: true
        // },
        id_comission_fk: {
            type: DataTypes.INTEGER,
            foreignKey: true
        }
    }, {});
    group.associate = function (models) {
        // associations can be defined here
        // group.hasMany(models.material_selection, {
        //     alias: 'group',
        //     foreignKey: 'id_group_fk'
        // })
        // group.hasMany(models.reagent_selection, {
        //     alias: 'group',
        //     foreignKey: 'id_group_fk'
        // })
        // group.hasMany(models.equipment_selection, {
        //     alias: 'group',
        //     foreignKey: 'id_group_fk'
        // })
        group.hasMany(models.student_group, {
            foreignKey: 'id_group_fk',
            sourceKey: 'id'
        })
        // group.hasOne(models.research, {
        //     foreignKey: 'id',
        //     sourceKey: 'id_research_fk'
        // })
        group.hasOne(models.comission, {
            foreignKey: 'id',
            sourceKey: 'id_comission_fk'
        })
        group.hasMany(models.group_research, {
            foreignKey: 'id_group_fk',
            sourceKey: 'id'
        })

    };
    return group;
};