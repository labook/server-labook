'use stric'

module.exports = (sequelize, DataTypes) => {
    const student_group = sequelize.define('student_group', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_student_fk: {
            type: DataTypes.INTEGER,
            foreignKey: true
        },
        id_group_fk: {
            type: DataTypes.INTEGER,
            foreignKey: true
        }
    }, {});
    student_group.associate = function (models) {
        student_group.hasOne(models.student, {
            foreignKey: 'id',
            sourceKey: 'id_student_fk'
        })
        student_group.hasOne(models.group, {
            foreignKey: 'id',
            sourceKey: 'id_group_fk'
        })
    }

    return student_group;
}