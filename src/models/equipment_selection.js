'use strict';
module.exports = (sequelize, DataTypes) => {
  const equipment_selection = sequelize.define('equipment_selection', {
    id_group_research_fk: DataTypes.INTEGER,
    id_equipment_fk: DataTypes.INTEGER,
    cant: DataTypes.INTEGER
  }, {});
  equipment_selection.associate = function (models) {
    // associations can be defined here
    equipment_selection.belongsTo(models.Equipment, {
      alias: 'Equipment',
      foreignKey: 'id_equipment_fk'
    })
    // equipment_selection.belongsTo(models.group, {
    //   alias: 'group',
    //   foreignKey: 'id_group_fk'
    // })
    equipment_selection.hasOne(models.group_research, {
      foreignKey: 'id',
      sorceKey: 'id_group_research_fk',
    });
  };
  return equipment_selection;
}; 