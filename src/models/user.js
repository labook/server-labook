'use strict';
module.exports = (sequelize, DataTypes) => {
    const user = sequelize.define('user', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_rol_fk: {
            type: DataTypes.INTEGER,
            foreignKey: true
        },
        id_student_fk: {
            type: DataTypes.INTEGER,
            foreignKey: true
        }, 
        id_teacher_fk: {
            type: DataTypes.INTEGER,
            foreignKey: true
        },
        dni: DataTypes.INTEGER,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
    }, {});
    user.associate = function (models) {

        user.hasOne(models.rol, {
            foreignKey: 'id',
            sourceKey: 'id_rol_fk'
        });
        user.hasOne(models.student, {
            foreignKey: 'id',
            sourceKey: 'id_student_fk'
        });
        user.hasOne(models.teacher, {
            foreignKey: 'id',
            sourceKey: 'id_teacher_fk'
        })

    };
    return user;
};