'use strict';

module.exports = (sequelize, DataTypes) => {
  const material_selection = sequelize.define('material_selection', {
    id_group_research_fk: DataTypes.INTEGER,
    id_material_fk: DataTypes.INTEGER,
    cant: DataTypes.INTEGER
  }, {});
  material_selection.associate = function (models) {
    // associations can be defined here
    material_selection.belongsTo(models.Material, {
      alias: 'Material',
      foreignKey: 'id_material_fk'
    });
    // material_selection.belongsTo(models.group, {
    //   alias: 'group',
    //   foreignKey: 'id_group_fk'
    // })
    material_selection.hasOne(models.group_research,{
      foreignKey:'id',
      sorceKey: 'id_group_research_fk'
    });
  };
  return material_selection;
}; 
