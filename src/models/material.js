'use strict';
module.exports = (sequelize, DataTypes) => {
  const Material = sequelize.define('Material', {
    name: DataTypes.STRING,
    image: DataTypes.TEXT,
    description: DataTypes.STRING,
    cant: DataTypes.INTEGER

  }, {});
  Material.associate = function (models) {
    // associations can be defined here
    Material.hasMany(models.material_selection, {
      alias: 'material_selections',
      foreingKey: 'id_material_fk'
    })
  };
  return Material;
}; 