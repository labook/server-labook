'use strict'

module.exports = (sequelize, DataTypes) => {
    const research_comission = sequelize.define('research_comission', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        // is_holder: DataTypes.BOOLEAN,
        id_research_fk: {
            foreignKey: true,
            type: DataTypes.INTEGER
        },
        id_comission_fk: {
            foreignKey: true,
            type: DataTypes.INTEGER
        },
        // id_research_fk: {
        //     foreignKey: true,
        //     type: DataTypes.INTEGER
        // }
    }, {});
    research_comission.associate = function (models) {
        research_comission.hasOne(models.research, {
            foreignKey: 'id',
            sourceKey: 'id_research_fk'
        })
        research_comission.hasOne(models.comission, {
            foreignKey: 'id',
            sourceKey: 'id_comission_fk'
        })
        // research_comission.hasOne(models.research, {
        //     foreignKey: 'id',
        //     sourceKey: 'id_research_fk'
        // })
    };

    return research_comission
}