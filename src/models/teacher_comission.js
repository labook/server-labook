'use strict'

module.exports = (sequelize, DataTypes) => {
    const teacher_comission = sequelize.define('teacher_comission', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        // is_holder: DataTypes.BOOLEAN,
        id_teacher_fk: {
            foreignKey: true,
            type: DataTypes.INTEGER
        },
        id_comission_fk: {
            foreignKey: true,
            type: DataTypes.INTEGER
        },
        // id_research_fk: {
        //     foreignKey: true,
        //     type: DataTypes.INTEGER
        // }
    }, {});
    teacher_comission.associate = function (models) {
        teacher_comission.hasOne(models.teacher, {
            foreignKey: 'id',
            sourceKey: 'id_teacher_fk'
        })
        teacher_comission.hasOne(models.comission, {
            foreignKey: 'id',
            sourceKey: 'id_comission_fk'
        })
        // teacher_comission.hasOne(models.research, {
        //     foreignKey: 'id',
        //     sourceKey: 'id_research_fk'
        // })
    };

    return teacher_comission
}