'use strict';
module.exports = (sequelize, DataTypes) => {
  const reagent_selection = sequelize.define('reagent_selection', {
    id_group_research_fk: DataTypes.INTEGER,
    id_reagent_fk: DataTypes.INTEGER,
    cant: DataTypes.INTEGER
  }, {});
  reagent_selection.associate = function (models) {
    // associations can be defined here
    reagent_selection.belongsTo(models.Reagent, {
      alias: 'Reagent',
      foreignKey: 'id_reagent_fk'
    })
    // reagent_selection.belongsTo(models.group, {
    //   alias: 'group',
    //   foreignKey: 'id_group_fk'
    // })
    reagent_selection.hasOne(models.group_research, {
      foreignKey: 'id',
      sourceKey: 'id_group_research_fk'
    });
  };
  return reagent_selection;
};

