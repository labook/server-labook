'use stric'

module.exports = (sequelize, DataTypes) => {
    const research = sequelize.define('research', {
        id: {
            primaryKey: true,
            autoIncrement: true,
            type: DataTypes.INTEGER
        },
        description: DataTypes.STRING,
        name: DataTypes.STRING,
        deliver_date: DataTypes.DATE
    }, {})
    research.associate = function (models) {
        research.hasMany(models.statement, {
            foreignKey: 'id_research_fk',
            sourceKey: 'id'
        });
        // research.hasMany(models.group, {
        //     foreignKey: 'id_research_fk',
        //     sourceKey: 'id'
        // });
        research.hasMany(models.research_comission, {
            foreignKey: 'id_research_fk',
            sourceKey: 'id'
        });
        research.hasMany(models.group_research, {
            foreignKey: 'id_research_fk',
            sourceKey: 'id'
        });
    }
    return research;
}