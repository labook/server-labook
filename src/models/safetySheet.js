'use strict';

module.exports = (sequelize, DataTypes) => {
    
    const safety_sheets = sequelize.define('safety_sheets', {
        safety_pdf: DataTypes.STRING,
        id_reagent_fk: DataTypes.INTEGER
    }, {});
    safety_sheets.associate = function (models) {
        safety_sheets.belongsTo(models.Reagent,
            {
                as: 'Reagent',
                foreignKey: 'id_reagent_fk'
            }
        )
    };
    return safety_sheets;
};