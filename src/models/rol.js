'use strict';
module.exports = (sequelize, DataTypes) => {
    const rol = sequelize.define('rol', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        rol_name: {
            type: DataTypes.INTEGER,
            foreignKey: true
        }
    }, {});
    rol.associate = function (models) {

        rol.hasMany(models.user, {
            foreignKey: 'id_rol_fk',
            sourceKey: 'id'
        });

    };
    return rol;
};