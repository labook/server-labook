module.exports = (sequelize, DataTypes) => {
    const comission = sequelize.define('comission', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        description: DataTypes.STRING,
        start_time: DataTypes.TIME,
        end_time: DataTypes.TIME,
        days: DataTypes.STRING,
        //DataTypes.ABSTRACT,
        id_subject_fk: {
            type: DataTypes.INTEGER,
            foreignKey: true
        },
        start_date: DataTypes.DATE,
        end_date: DataTypes.DATE,
        active: DataTypes.BOOLEAN
    }, {});
    comission.associate = function (models) {
        // associations can be defined here
        comission.hasOne(models.subject, {
            //alias: 'subject',
            foreignKey: 'id',
            sourceKey: 'id_subject_fk'
        })
        comission.hasMany(models.student, {
            foreignKey: 'id_comission_fk',
            sourceKey: 'id'
        })
        comission.hasMany(models.teacher_comission, {
            foreignKey: 'id_comission_fk',
            sourceKey: 'id'
        })
        comission.hasMany(models.research_comission, {
            foreignKey: 'id_comission_fk',
            sourceKey: 'id'
        })
        comission.hasMany(models.group, {
            foreignKey: "id_comission_fk",
            sourceKey: "id"
        })

    };
    return comission;
}; 