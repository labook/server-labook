'use strict';
module.exports = (sequelize, DataTypes) => {
    const student = sequelize.define('student', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: DataTypes.STRING,
        lastname: DataTypes.STRING,
        dni: DataTypes.INTEGER,
        id_comission_fk: {
            type: DataTypes.INTEGER,
            foreignKey: true
        },
    }, {});
    student.associate = function (models) {
        // associations can be defined here
        student.hasOne(models.comission, {
            foreignKey: 'id',
            sourceKey: 'id_comission_fk'
        });
        student.hasMany(models.student_group, {
            foreignKey: 'id_student_fk',
            sourceKey: 'id'
        });
        student.hasMany(models.user, {
            foreignKey: 'id_student_fk',
            sourceKey: 'id'
        });
    };
    return student;
};