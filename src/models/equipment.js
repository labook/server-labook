'use strict';
module.exports = (sequelize, DataTypes) => {
  const Equipment = sequelize.define('Equipment', {
    name: DataTypes.STRING,
    image: DataTypes.TEXT,
    description: DataTypes.STRING,
    cant: DataTypes.INTEGER
  }, {});
  Equipment.associate = function (models) {
    // associations can be defined here
    Equipment.hasMany(models.equipment_selection, {
      alias: 'equipment_selections',
      foreingKey: 'id_equipment_fk'
    })
  };
  return Equipment;
};