'use strict';
module.exports = (sequelize, DataTypes) => {
    const Reagent = sequelize.define('Reagent', {
        name: DataTypes.STRING,
        cas: DataTypes.INTEGER,
        image: DataTypes.TEXT,
        description: DataTypes.STRING,
        cant: DataTypes.INTEGER
    }, {});
    Reagent.associate = function (models) {
        // associations can be defined here
        Reagent.hasMany(models.safety_sheets,
            {
                as: 'safety_sheet',
                foreignKey: 'id_reagent_fk'
            }
        )
        Reagent.hasMany(models.reagent_selection,
            {
                alias: 'reagent_selections',
                foreignKey: 'id_reagent_fk'
            }
        )
    };
    return Reagent;
};