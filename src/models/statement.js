'use stric'

module.exports = (sequelize, DataTypes) => {
    const statement = sequelize.define('statement', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        statement_pdf: DataTypes.TEXT,
        id_research_fk: {
            type: DataTypes.INTEGER,
            foreignKey: true
        }
    }, {})
    statement.associate = function (models) {
        statement.belongsTo(models.research, {
            foreignKey: 'id',
            sourceKey: 'id_research_fk'
        })
    }
    return statement;
}