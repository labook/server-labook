const jwt = require('jsonwebtoken');

exports.validateJwt = (req, res, next) => {

    //Read token from header
    const token = req.headers['authorization'];

    if (!token) {
        return res.status(401).json({
            msg: 'Unauthorized'
        })
    }

    try {

        const { id } = jwt.verify(token, 'secret');
        req.id = id;
        next();

    } catch (error) {
        return res.status(401).json({
            msg: 'Incorrect Token'
        })
    }

}
