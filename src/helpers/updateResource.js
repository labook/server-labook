const models = require('../models');

exports.updateResource = async (id, type, cant) => {
    switch (type) {
        case 'reagent':

            return await updateModel(id, models.Reagent, cant);
        case 'equipment':

            return await updateModel(id, models.Equipment, cant);
        case 'material':

            return await updateModel(id, models.Material, cant);

        default:
            break;
    }
}

const updateModel = async(id, models, cant) => {
    const dataDB = await models.findOne({
        where: { id }
    });
    return await dataDB.update({
        cant
    });
}