require("dotenv").config();
const express = require("express");
const path = require("path");
const morgan = require("morgan");
const { errorMiddleware } = require("./middlewares/errorHandler");
const cors = require("cors");
const multer = require("multer");
const { v4: uuidv4 } = require("uuid");

const storage = multer.diskStorage({
  destination: path.join(__dirname, "public/uploads"),
  filename: (req, file, cb) => {
    cb(null, uuidv4() + path.extname(file.originalname));
  },
});

//Import routes
const indexRoutes = require("./routes");

//Inicializations
const app = express();

//Settings
app.set("port", process.env.PORT || 8000);

//Middlewares
app.use(express.json());
app.use(morgan("dev"));
app.use(cors());

// MULTER
app.use(
  multer({
    storage,
    dest: path.join(__dirname, "public/uploads"),
  }).single("pdf")
);
// MULTER

// Routes
indexRoutes.map(route => 
    app.use('/', route)
)

//Static files
app.use(express.static(path.join(__dirname, "public")));

//ErrorHandler
app.use(errorMiddleware);

app.listen(app.get("port"), () => {
  console.log("Server running  localhost:" + app.get("port"));
});

module.exports = app;
